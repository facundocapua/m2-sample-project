FROM glamit-base-php-74

RUN mkdir /app

WORKDIR /app

#COPY /Users/facundocapua/.composer/auth.json /var/www/.composer-global/.composer/auth.json
COPY ./ /app

RUN php -d memory_limit=-1 /usr/local/bin/composer install --prefer-dist --no-interaction --optimize-autoloader  --no-dev

RUN php -d memory_limit=-1 bin/magento setup:di:compile
RUN JOBS=$(grep -c processor /proc/cpuinfo | tr -d '\n') && \
    php -d memory_limit=-1 bin/magento setup:static-content:deploy -f --jobs=$JOBS

RUN chmod -R 777 /app

RUN cp app/etc/env.php.deploy app/etc/env.php